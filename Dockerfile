# Build from the ycast-docker container
# build with:
# docker build --compress -t myrepo/ycast-docker .
# run with:
# docker run -d --name vtuner-ycast -p 8080:80 myrepro/ycast-docker

FROM alpine:latest

#Variables
#YC_VERSION version of ycast software
#YC_STATIONS path an name of the indiviudual stations.yml e.g. /ycast/stations/stations.yml
#YC_DEBUG turn ON or OFF debug output of ycast server else only start /bin/sh
#YC_PORT port ycast server listens to, e.g. 80
ENV YC_VERSION 1.1.0
ENV YC_STATIONS /opt/ycast/stations.yml
ENV YC_DEBUG OFF
ENV YC_PORT 80

RUN apk --no-cache update && \
    apk --no-cache upgrade && \
    apk add --no-cache python3 && \
    apk add --no-cache zlib-dev && \
    apk add --no-cache jpeg-dev && \
    apk add --no-cache build-base && \
    apk add --no-cache python3-dev && \
    apk add --no-cache py3-pip && \
    apk add --no-cache py3-requests && \
    apk add --no-cache py3-flask && \
    apk add --no-cache py3-pyaml && \
    apk add --no-cache py3-pillow  && \
    mkdir /opt/ycast && \
    apk del python3-dev && \
    apk del build-base && \
    apk del zlib-dev && \
    apk add --no-cache curl && \
    curl -L https://github.com/milaq/YCast/archive/$YC_VERSION.tar.gz \
    | tar xvzC /opt/ycast && \
    apk del curl
WORKDIR /opt/ycast/YCast-$YC_VERSION
COPY bootstrap.sh /opt
EXPOSE $YC_PORT/tcp

RUN ["sh", "-c", "chmod +x /opt/bootstrap.sh"]
ENTRYPOINT ["/opt/bootstrap.sh"]

# Healthcheck
#HEALTHCHECK CMD ps aux | grep -i "python3 -m ycast -c $YC_STATIONS -p $YC_PORT" | grep -v grep || exit 1





